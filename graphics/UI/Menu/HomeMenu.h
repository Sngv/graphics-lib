#ifndef HOMEMENU_H_
#define HOMEMENU_H_

#include "../../Screen/Screen.h"
#include "../../widgets/WidgetComponent/Button.h"
#include "../../widgets/WidgetComponent/Frame.h"
#include "../../actions/SwitchFramesAction.h"

class HomeMenu: public Frame {
public:
	HomeMenu();
	virtual ~HomeMenu();

	Button *callNumber;
	Button *sendSMS;

	void AddActionToButton();
};

#endif /* HOMEMENU_H_ */
