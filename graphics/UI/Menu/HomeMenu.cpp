#include "HomeMenu.h"

HomeMenu::HomeMenu() :
		Frame() {
	// TODO Auto-generated constructor stub
	this->setHeight(280);
	callNumber = new Button();
	callNumber->setWidth(236), callNumber->setHeight(40);
	callNumber->setUpperLeft(2, 2);
	callNumber->setText("Call");

	sendSMS = new Button();
	sendSMS->setWidth(236), sendSMS->setHeight(40);
	sendSMS->setUpperLeft(2, 44);
	sendSMS->setText("Send SMS");

	addWidget(callNumber);
	addWidget(sendSMS);
}

HomeMenu::~HomeMenu() {
	// TODO Auto-generated destructor stub
}

void HomeMenu::AddActionToButton() {
	SwitchFramesAction *callToCallingNumber = new SwitchFramesAction(3);
	callNumber->setActionReleased(callToCallingNumber);

	SwitchFramesAction *callToSendSMS = new SwitchFramesAction(4);
	sendSMS->setActionReleased(callToSendSMS);

}

