#ifndef FIXEDMENU_H_
#define FIXEDMENU_H_

#include "../../widgets/WidgetComponent/Button.h"
#include "../../widgets/WidgetComponent/Frame.h"
#include "../../actions/SwitchFramesAction.h"
#include "../../actions/ActionOnButton/FixedBackButton.h"

class FixedMenu: public Frame {
public:
	FixedMenu();
	virtual ~FixedMenu();

	Button *back;
	Button *lock;
	Button *home;

	void addFrame();
	void AddActionToButton();
};

#endif /* FIXEDMENU_H_ */
