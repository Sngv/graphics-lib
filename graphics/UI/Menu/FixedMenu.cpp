#include "FixedMenu.h"

FixedMenu::FixedMenu() {
	this->setUpperLeft(0, 280);
	this->setHeight(40), this->setWidth(240);

	lock = new Button();
	lock->setUpperLeft(0, 0);
	lock->setHeight(40), lock->setWidth(80);
	lock->setText("Lock");

	home = new Button();
	home->setUpperLeft(80, 0);
	home->setHeight(40), home->setWidth(80);
	home->setText("Home");

	back = new Button();
	back->setUpperLeft(160, 0);
	back->setHeight(40), back->setWidth(80);
	back->setText("Back");

	addFrame();
}

FixedMenu::~FixedMenu() {

}

void FixedMenu::addFrame() {
	this->addWidget(back);
	this->addWidget(lock);
	this->addWidget(home);
}

void FixedMenu::AddActionToButton() {
	SwitchFramesAction *moveToHome = new SwitchFramesAction(1);
	home->setActionReleased(moveToHome);

	//lock
	//MovingFrames *loac = new MovingFrames(1, 3);
	//myButton1->setActionReleased(call);

	FixedBackButton *backAction = new FixedBackButton();
	back->setActionReleased(backAction);
}
