#ifndef ABSTRACTKEYPAD_H_
#define ABSTRACTKEYPAD_H_

#include "../../widgets/WidgetComponent/Button.h"
#include "../../widgets/WidgetComponent/ContainerWidget.h"
#include "../../widgets/WidgetComponent/Frame.h"
#include "../../widgets/WidgetComponent/TextField.h"
#include "../../widgets/Widget.h"
#include "../../actions/AddToTextFieldAction.h"
#include "../../actions/SwitchFramesAction.h"
#include "../../actions/BackSpaceAction.h"

class AbstractKeypad: public Frame {
public:
	Button *button[10];
	TextField *textInput;
	BackSpaceAction *back;
	Button *ActionBackSpaceButtom;
	AbstractAction *actionAdd[12];

	AbstractKeypad();
	virtual ~AbstractKeypad();
	void show();
	string toString(int num);

};

#endif /* ABSTRACTKEYPAD_H_ */
