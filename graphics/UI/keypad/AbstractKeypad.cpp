#include "AbstractKeypad.h"

AbstractKeypad::AbstractKeypad() {
	this->setHeight(280);
}

AbstractKeypad::~AbstractKeypad() {
	// TODO Auto-generated destructor stub
}

string AbstractKeypad::toString(int num) {
	if (num == 0)
		return "0";
	string numberToString = "";
	while (num != 0) {
		numberToString += num % 10 + '0', num /= 10;
	}
	return numberToString;
}
