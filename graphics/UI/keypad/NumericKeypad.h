#ifndef NUMERICKEYPAD_H_
#define NUMERICKEYPAD_H_

#include "../../widgets/WidgetComponent/Button.h"
#include "../../widgets/WidgetComponent/ContainerWidget.h"
#include "../../widgets/WidgetComponent/Frame.h"
#include "../../widgets/WidgetComponent/TextField.h"
#include "../../widgets/Widget.h"
#include "../../actions/BackSpaceAction.h"
#include "AbstractKeypad.h"
#include "../../actions/AbstractAction.h"
#include "../../actions/SwitchFramesAction.h"

class NumericKeypad: public AbstractKeypad {
public:
	Button *call;
	NumericKeypad();
	virtual ~NumericKeypad();
	void AddActionToButton();
};

#endif /* NUMERICKEYPAD_H_ */
