#include "AlphaKeypad.h"

AlphaKeypad::~AlphaKeypad() {
	// TODO Auto-generated destructor stub
}

AlphaKeypad::AlphaKeypad() {
	this->setHeight(280);

	textInput = new TextField();
	textInput->setWidth(230), textInput->setHeight(40);
	textInput->setText("Enter Number");
	textInput->setUpperLeft(4, 1);
	string count = "a";
	for (int i = 0; i < 9; i++) {
		button[i + 1] = new Button();
		button[i + 1]->setWidth(75), button[i + 1]->setHeight(55);
		button[i + 1]->setUpperLeft(i % 3 * 80 + (1 * i / 3 + 3),
				40 + (i / 3 * 60) + (4 * i / 3));
		button[i + 1]->setText(count);
		button[i + 1]->charText = count;
		count[0] += 3;

	}
	ActionBackSpaceButtom = new Button();
	ActionBackSpaceButtom->setWidth(75), ActionBackSpaceButtom->setHeight(50);
	ActionBackSpaceButtom->setUpperLeft(161, 227);
	ActionBackSpaceButtom->setText("<--");

	button[0] = new Button();
	button[0]->setWidth(75), button[0]->setHeight(50);
	button[0]->setUpperLeft(81, 227);
	button[0]->setText(" 0");

	sendSms = new Button();
	sendSms->setWidth(75), sendSms->setHeight(50);
	sendSms->setUpperLeft(0, 227);
	sendSms->setText("Send");

	back = new BackSpaceAction(textInput);
	ActionBackSpaceButtom->setActionReleased(back);

	for (int i = 0; i <= 9; i++)
		this->addWidget(button[i]);

	this->addWidget(sendSms);
	this->addWidget(ActionBackSpaceButtom);
	this->addWidget(textInput);

}

void AlphaKeypad::AddActionToButton() {
	string count = "a";
	for (int i = 1; i < 10; i++) {
		actionAdd[i] = new AlphaButtonAction(textInput, button[i]);
		button[i]->setActionReleased(actionAdd[i]);
		count[0] += 3;
	}

	movingToSendingMode = new SwitchFramesAction(5);
	sendSms->setActionReleased(movingToSendingMode);
}
