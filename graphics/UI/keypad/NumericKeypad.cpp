#include "NumericKeypad.h"

NumericKeypad::NumericKeypad() {
	this->setHeight(280);

	textInput = new TextField();
	textInput->setWidth(230), textInput->setHeight(40);
	textInput->setText("Enter Number");
	textInput->setUpperLeft(4, 1);

	for (int i = 0; i < 9; i++) {
		button[i + 1] = new Button();
		button[i + 1]->setWidth(75), button[i + 1]->setHeight(55);
		button[i + 1]->setUpperLeft(i % 3 * 80 + (1 * i / 3 + 3),
				40 + (i / 3 * 60) + (4 * i / 3));
		button[i + 1]->setText(" " + toString(i + 1));
	}

	ActionBackSpaceButtom = new Button();
	ActionBackSpaceButtom->setWidth(75), ActionBackSpaceButtom->setHeight(50);
	ActionBackSpaceButtom->setUpperLeft(161, 227);
	ActionBackSpaceButtom->setText("<--");

	button[0] = new Button();
	button[0]->setWidth(75), button[0]->setHeight(50);
	button[0]->setUpperLeft(81, 227);
	button[0]->setText(" 0");

	call = new Button();
	call->setWidth(75), call->setHeight(50);
	call->setUpperLeft(0, 227);
	call->setText("call");

	for (int i = 0; i <= 9; i++)
		this->addWidget(button[i]);

	this->addWidget(call);
	this->addWidget(ActionBackSpaceButtom);
	this->addWidget(textInput);

}

NumericKeypad::~NumericKeypad() {
// TODO Auto-generated destructor stub
}

void NumericKeypad::AddActionToButton() {
	for (int i = 0; i < 10; i++) {
		actionAdd[i] = new AddToTextFieldAction(textInput, toString(i));
		button[i]->setActionReleased(actionAdd[i]);
	}

	back = new BackSpaceAction(textInput);
	ActionBackSpaceButtom->setActionReleased(back);

	SwitchFramesAction * toCallingFrame = new SwitchFramesAction(2);
	call->setActionReleased(toCallingFrame);
}
