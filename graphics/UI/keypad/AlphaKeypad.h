#ifndef ALPHAKEYPAD_H_
#define ALPHAKEYPAD_H_

#include "../../widgets/WidgetComponent/Button.h"
#include "../../widgets/WidgetComponent/ContainerWidget.h"
#include "../../widgets/WidgetComponent/Frame.h"
#include "../../widgets/WidgetComponent/TextField.h"
#include "../../widgets/Widget.h"
#include "../../actions/BackSpaceAction.h"
#include "AbstractKeypad.h"
#include "../../actions/AbstractAction.h"
#include "../../actions/AlphaButtonAction.h"

class AlphaKeypad: public AbstractKeypad {
public:

	AlphaKeypad();
	virtual ~AlphaKeypad();

	Button *sendSms;

	SwitchFramesAction *movingToSendingMode;
	void AddActionToButton();

};

#endif /* ALPHAKEYPAD_H_ */
