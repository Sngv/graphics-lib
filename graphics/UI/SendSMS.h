/*
 * SendSMS.h
 *
 *  Created on: Sep 7, 2014
 *      Author: sngv
 */

#ifndef SENDSMS_H_
#define SENDSMS_H_

#include "../widgets/WidgetComponent/Frame.h"
#include "../widgets/WidgetComponent/TextView.h"

class SendSMS: public Frame {
public:
	SendSMS();
	virtual ~SendSMS();

	string userNumber;
	TextView *txtview;
};

#endif /* SENDSMS_H_ */
