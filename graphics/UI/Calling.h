#ifndef CALLING_H_
#define CALLING_H_

#include "../widgets/WidgetComponent/Frame.h"
#include "../widgets/WidgetComponent/TextView.h"

class Calling: public Frame {
public:
	Calling();
	virtual ~Calling();
	string callNumber;
	TextView *txtview;
};

#endif /* CALLING_H_ */
