#ifndef DRAWABLE_H
#define DRAWABLE_H

#include "../util/Constants.h"
#include "../util/Point.h"
#include "../DrawingTools/DrawingTools.h"
#include "../driver/winbgim.h"
#include "../actions/AbstractAction.h"
#include "../color/Color.h"
#include <string>
#include <iostream>
#include <vector>

using namespace std;

class Point;
class AbstractAction;

class Widget {

public:
	int id;
	int size;
	font_names font;
	Color *color;
	string type;
	int numberOfHit;

protected:
	// drawable properties
	Point * upperleft;
	int width;
	int height;
	State state;
	string text;

	// actions
	AbstractAction * actionPressed;
	AbstractAction * actionReleased;
	AbstractAction * actionGotFocus;

public:
	// basic operations
	Widget();
	virtual ~Widget();
	bool equals(Widget * drawable);

public:
	// operations
	virtual void draw(DrawingTools * drawingTool);
	virtual void drawPressed(DrawingTools * drawingTool);
	virtual void drawReleased(DrawingTools * drawingTool);
	virtual void drawGotFocus(DrawingTools * drawingTool);
	virtual void drawNormal(DrawingTools * drawingTool);

	virtual void setHeight(int height);
	virtual void setWidth(int width);
	virtual void setUpperLeft(int x, int y);
	virtual void setUpperLeft(Point * point);
	virtual Point * getUpperLeft();
	virtual Widget * contains(int x, int y);
	virtual void setState(State value);
	State getState();
	virtual void handleKeyboardInput(char c);

	void setText(string value);
	string getText();

	// actions
	virtual void fireAction(State value);
	void setActionPressed(AbstractAction * action);
	void setActionReleased(AbstractAction * action);
	void setActionGotFocus(AbstractAction * action);

	//Color :D
//	string NORMAL_BUTTON_COLOR_TEXT;
//	string NORMAL_BUTTON_OUTER_BORDER;
//	string NORMAL_BUTTON_INNER_BORDER;
//	string NORMAL_BUTTON_INNER_COLOR;

	virtual void setPressedColor(string pressedText, string pressedInnerBorder,
			string pressedInnerColor, string pressedOuterBorder);

	virtual void setReleasedColor(string releasedText,
			string releasedInnerBorder, string releasedInnerColor,
			string releasedOuterBorder);

	virtual void setGotFocusColor(string gotFocusText,
			string gotFocusInnerBorder, string gotFocusInnerColor,
			string gotFocusOuterBorder);

	virtual void setDeafult(string deafultText, string deafultInnerBorder,
			string deafultInnerColor, string deafultOuterBorder);

	virtual void putDeauflt();
};

#endif // DRAWABLE_H
