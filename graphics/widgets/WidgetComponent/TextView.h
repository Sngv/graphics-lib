/*
 * TextView.h
 *
 *  Created on: Sep 2, 2014
 *      Author: sngv
 */

#ifndef TEXTVIEW_H_
#define TEXTVIEW_H_

#include "../Widget.h"
#include "../../color/Color.h"
#include "../../DrawingTools/DrawingTools.h"

class TextView: public Widget {
public:
	TextView();
	virtual ~TextView();

	Color *color;

	string textPad;
	virtual void drawPressed(DrawingTools *drawingTools);
	virtual void drawReleased(DrawingTools *drawingTools);
	virtual void drawGotFocus(DrawingTools *drawingTools);

	virtual void handleKeyboardInput(char c);
	virtual void setText(string value);
	virtual void drawNormal(DrawingTools * drawingTool);

};

#endif /* TEXTVIEW_H_ */
