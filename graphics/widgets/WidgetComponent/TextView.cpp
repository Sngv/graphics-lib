/*
 * TextView.cpp
 *
 *  Created on: Sep 2, 2014
 *      Author: sngv
 */

#include "TextView.h"

TextView::TextView() {

	putDeauflt();
	text = "Calling";
	upperleft = new Point(100, 160);
	width = 160;
	height = COMMON_HEIGHT;

	color = new Color();

	color->COLOR_TEXT = "WHITE";
	color->OUTER_BORDER = "WHITE";
	color->INNER_BORDER = "WHITE";
	color->INNER_COLOR = "BLACK";

}

TextView::~TextView() {
	// TODO Auto-generated destructor stub
}

void TextView::setText(string value) {
	textPad = value;
}

void TextView::handleKeyboardInput(char tmp) {

}

void TextView::drawGotFocus(DrawingTools* drawingTools) {
	drawNormal(drawingTools);
}

void TextView::drawPressed(DrawingTools* drawingTools) {
	drawNormal(drawingTools);
}

void TextView::drawReleased(DrawingTools* drawingTools) {
	drawNormal(drawingTools);
}

void TextView::drawNormal(DrawingTools *drawingTool) {
	font_names tmp = font;
	int sz = size;
	settextstyle(font, HORIZ_DIR, size);
	drawingTool->setColorBG(color->innerColor()); // background txt
	drawingTool->setColorFG(color->colorText()); // text
	drawingTool->drawText((char *) textPad.c_str(), upperleft->getX() + 10,
			upperleft->getY() + 10);
	settextstyle(tmp, HORIZ_DIR, sz);
}
