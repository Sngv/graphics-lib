#ifndef CONTAINER_WIDGET_H
#define CONTAINER_WIDGET_H
#include "../Widget.h" // inheriting class's header file
#include "../../DrawingTools/DrawingTools.h"
#include "../../util/Point.h"
#include "../../Screen/Screen.h"

#include <vector>
using namespace std;

/*
 * No description
 */
class Point;
class ContainerWidget: public Widget {
private:
	vector<Widget *> * widgetsVector;

public:
	// class constructor
	ContainerWidget();
	// class destructor
	~ContainerWidget();

	virtual Widget * contains(int x, int y);
	virtual void addWidget(Widget * widget);
	virtual void draw(DrawingTools * drawingTool);

};
#endif // CONTAINER_WIDGET_H
