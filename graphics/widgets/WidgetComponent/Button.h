/*
 * Button.h
 *
 *  Created on: Aug 30, 2014
 *      Author: sngv
 */

#ifndef BUTTON_H_
#define BUTTON_H_

#include "../Widget.h"
#include "../../util/Point.h"
#include "../../DrawingTools/DrawingTools.h"
#include "../../color/Color.h"

class Point;
class Button: public Widget {
private:
	vector<colors> colorBackGround, colorRectangle, colorText;

public:
	Button();
	~Button();

	string charText;
	int counterPress;
	Color *color;

	virtual void drawPressed(DrawingTools *drawingTools);
	virtual void drawReleased(DrawingTools *drawingTools);
	virtual void drawGotFocus(DrawingTools *drawingTools);
	virtual void drawNormal(DrawingTools *drawingTools);
	void drawNoramlCircule(); // still undefined
};

#endif /* BUTTON_H_ */
