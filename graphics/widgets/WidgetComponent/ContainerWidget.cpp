#include "ContainerWidget.h"

ContainerWidget::ContainerWidget() {

	color = new Color();

	widgetsVector = new vector<Widget *>;
	upperleft = new Point(0, 0);
	width = 0, height = 0;
}

ContainerWidget::~ContainerWidget() {

}

Widget *ContainerWidget::contains(int x, int y) {
	bool x1 = upperleft->getX() <= x;
	bool x2 = upperleft->getX() + width >= x;
	bool y1 = upperleft->getY() <= y;
	bool y2 = upperleft->getY() + height >= y;

	if (x1 && x2 && y1 && y2) {
		for (unsigned int i = 0; i < widgetsVector->size(); i++) {
			Widget *widget = widgetsVector->at(i);
			if (widget->contains(x, y) != NULL) {
				*Screen::idButtonPress = widget->id;
				cout << "returning child , id = " << widget->id << endl;
				cout << widget->getText() << endl;
				return widget;
			}
		}
		cout << "returning its self " << endl;
		return this;
	}
	return NULL;
}

void ContainerWidget::addWidget(Widget *widget) {
	widget->getUpperLeft()->setX(
			widget->getUpperLeft()->getX() + this->getUpperLeft()->getX());

	widget->getUpperLeft()->setY(
			widget->getUpperLeft()->getY() + this->getUpperLeft()->getY());

	widgetsVector->push_back(widget);
}

void ContainerWidget::draw(DrawingTools *drawingTool) {
	if (state == NORMAL)
		drawNormal(drawingTool);

	if (state == GOT_FOCUS)
		drawGotFocus(drawingTool);

	if (state == PRESSED)
		drawPressed(drawingTool);

	if (state == RELEASED)
		drawReleased(drawingTool);

	for (unsigned int i = 0; i < widgetsVector->size(); i++) {
		Widget *widget = widgetsVector->at(i);
		widget->draw(drawingTool);
	}
}
