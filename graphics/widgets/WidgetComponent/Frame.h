/*
 * Frame.h
 *
 *  Created on: Aug 30, 2014
 *      Author: sngv
 */

#ifndef FRAME_H_
#define FRAME_H_

#include "../Widget.h"
#include "../../util/Point.h"
#include "../../DrawingTools/DrawingTools.h"
#include "../../color/Color.h"
#include "ContainerWidget.h"

class Point;
class Frame: public ContainerWidget {
public:
	Frame();
	~Frame();
	virtual void drawNormal(DrawingTools *drawingTools);
	virtual void setState(State value);
};

#endif /* FRAME_H_ */
