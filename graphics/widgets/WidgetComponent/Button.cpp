#include "Button.h"

Button::Button() {
	putDeauflt();
	size = 2;
	font = DEFAULT_FONT;
	text = "Button?";
	upperleft = new Point(0, 0);
	width = 160;
	height = COMMON_HEIGHT;
	color = new Color();
	numberOfHit = 3;
	counterPress = 0;
}

Button::~Button() {

}

void Button::drawPressed(DrawingTools *drawingTool) {
	drawingTool->setColorBG(color->pressed->outerBorder());
	drawingTool->setColorFG(color->pressed->outerBorder());
	drawingTool->drawRectangle(upperleft->getX(), upperleft->getY(),
			upperleft->getX() + width, upperleft->getY() + height);

	// inner rectangle
	drawingTool->setColorBG(color->pressed->innerColor());	//
	drawingTool->setColorFG(color->pressed->innerBoard()); // inner Border
	drawingTool->drawRectangle(upperleft->getX() + COMMON_MARGIN,
			upperleft->getY() + COMMON_MARGIN,
			upperleft->getX() + width - COMMON_MARGIN,
			upperleft->getY() + height - COMMON_MARGIN);

	// text
	font_names tmp = font;
	int sz = size;
	settextstyle(font, HORIZ_DIR, size);
	drawingTool->setColorBG(color->pressed->colorText()); // writing
	drawingTool->setColorFG(color->pressed->innerColor()); // the same inner recatnage
	drawingTool->drawText((char *) text.c_str(), upperleft->getX() + 10,
			upperleft->getY() + 10);
	settextstyle(tmp, HORIZ_DIR, sz);
}

void Button::drawReleased(DrawingTools *drawingTool) {

	drawingTool->setColorBG(color->released->outerBorder());
	drawingTool->setColorFG(color->released->outerBorder());
	drawingTool->drawRectangle(upperleft->getX(), upperleft->getY(),
			upperleft->getX() + width, upperleft->getY() + height);

	// inner rectangle
	drawingTool->setColorBG(color->released->innerColor());	//
	drawingTool->setColorFG(color->released->innerBoard()); // inner Border
	drawingTool->drawRectangle(upperleft->getX() + COMMON_MARGIN,
			upperleft->getY() + COMMON_MARGIN,
			upperleft->getX() + width - COMMON_MARGIN,
			upperleft->getY() + height - COMMON_MARGIN);

	// text
	font_names tmp = font;
	int sz = size;
	settextstyle(font, HORIZ_DIR, size);
	drawingTool->setColorBG(color->released->innerColor()); // writing
	drawingTool->setColorFG(color->released->colorText()); // the same inner recatnage
	drawingTool->drawText((char *) text.c_str(), upperleft->getX() + 10,
			upperleft->getY() + 10);
	settextstyle(tmp, HORIZ_DIR, sz);
}

void Button::drawGotFocus(DrawingTools *drawingTool) {

	drawingTool->setColorBG(color->gotFocus->outerBorder());
	drawingTool->setColorFG(color->gotFocus->outerBorder());
	drawingTool->drawRectangle(upperleft->getX(), upperleft->getY(),
			upperleft->getX() + width, upperleft->getY() + height);

	// inner rectangle
	drawingTool->setColorBG(color->gotFocus->innerColor());	//
	drawingTool->setColorFG(color->gotFocus->innerBoard()); // inner Border
	drawingTool->drawRectangle(upperleft->getX() + COMMON_MARGIN,
			upperleft->getY() + COMMON_MARGIN,
			upperleft->getX() + width - COMMON_MARGIN,
			upperleft->getY() + height - COMMON_MARGIN);

	// text
	font_names tmp = font;
	int sz = size;
	settextstyle(font, HORIZ_DIR, size);
	drawingTool->setColorBG(color->gotFocus->innerColor()); // writing
	drawingTool->setColorFG(color->gotFocus->colorText()); // the same inner recatnage
	drawingTool->drawText((char *) text.c_str(), upperleft->getX() + 10,
			upperleft->getY() + 10);
	settextstyle(tmp, HORIZ_DIR, sz);
}

void Button::drawNormal(DrawingTools *drawingTool) {

	// outer BORDER
	drawingTool->setColorBG(color->outerBorder());
	drawingTool->setColorFG(color->outerBorder());
	drawingTool->drawRectangle(upperleft->getX(), upperleft->getY(),
			upperleft->getX() + width, upperleft->getY() + height);

	// inner rectangle
	drawingTool->setColorBG(color->innerColor());	//
	drawingTool->setColorFG(color->innerBoard()); // inner Border
	drawingTool->drawRectangle(upperleft->getX() + COMMON_MARGIN,
			upperleft->getY() + COMMON_MARGIN,
			upperleft->getX() + width - COMMON_MARGIN,
			upperleft->getY() + height - COMMON_MARGIN);

	// text
	font_names tmp = font;
	int sz = size;
	settextstyle(font, HORIZ_DIR, size);
	drawingTool->setColorBG(color->innerColor()); // writing
	drawingTool->setColorFG(color->colorText()); // the same inner recatnage
	drawingTool->drawText((char *) text.c_str(), upperleft->getX() + 10,
			upperleft->getY() + 10);
	settextstyle(tmp, HORIZ_DIR, sz);
}
