
#include "Widget.h" // class's header file

Widget::Widget() {
	state = NORMAL;
	id = getNextID();
	size = 2;
	font = DEFAULT_FONT;
	actionPressed = NULL;
	actionReleased = NULL;
	actionGotFocus = NULL;
	numberOfHit = 3;
	color = new Color();
}

void Widget::setPressedColor(string pressedText, string pressedInnerBorder,
		string pressedInnerColor, string pressedOuterBorder) {
}

void Widget::setReleasedColor(string releasedText, string releasedInnerBorder,
		string releasedInnerColor, string releasedOuterBorder) {
}

void Widget::setGotFocusColor(string gotFocusText, string gotFocusInnerBorder,
		string gotFocusInnerColor, string gotFocusOuterBorder) {
}

void Widget::setDeafult(string deafultText, string deafultInnerBorder,
		string deafultInnerColor, string deafultOuterBorder) {
}

Widget::~Widget() {
}

void Widget::setHeight(int height) {
	this->height = height;
}

void Widget::putDeauflt() {
	size = 2;
	font = DEFAULT_FONT;

}

void Widget::setWidth(int width) {
	this->width = width;
}

bool Widget::equals(Widget * drawable) {
	if (drawable == NULL)
		return false;
	return this->id == drawable->id;
}

void Widget::draw(DrawingTools * drawingTool) {
	if (state == NORMAL) {
		drawNormal(drawingTool);
		return;
	}
	if (state == GOT_FOCUS) {
		drawGotFocus(drawingTool);
		return;
	}
	if (state == PRESSED) {
		drawPressed(drawingTool);
		return;
	}
	if (state == RELEASED) {
		drawReleased(drawingTool);
		return;
	}
}
void Widget::drawPressed(DrawingTools * drawingTool) {
}
void Widget::drawReleased(DrawingTools * drawingTool) {
}
void Widget::drawGotFocus(DrawingTools * drawingTool) {
}
void Widget::drawNormal(DrawingTools * drawingTool) {
}

void Widget::setUpperLeft(int x, int y) {
	upperleft = new Point(x, y);
}
void Widget::setUpperLeft(Point * point) {
	setUpperLeft(point->getX(), point->getY());
}

Point * Widget::getUpperLeft() {
	return upperleft;
}

Widget * Widget::contains(int paramX, int paramY) {
	bool x1 = upperleft->getX() <= paramX;
	bool x2 = upperleft->getX() + width >= paramX;
	bool y1 = upperleft->getY() <= paramY;
	bool y2 = upperleft->getY() + height >= paramY;

	if (x1 && x2 && y1 && y2)
		return this;
	return NULL;

}

void Widget::setState(State value) {
	state = value;
}
State Widget::getState() {
	return state;
}

void Widget::setText(string value) {
	text = value;
}

string Widget::getText() {
	return text;
}

void Widget::handleKeyboardInput(char c) {

}

void Widget::fireAction(State value) {
	if (value == PRESSED && actionPressed != NULL) {
		actionPressed->fire();
		return;
	}
	if (value == RELEASED && actionReleased != NULL) {
		actionReleased->fire();
		return;
	}
	if (value == GOT_FOCUS && actionGotFocus != NULL) {
		actionGotFocus->fire();
		return;
	}
}

void Widget::setActionPressed(AbstractAction * action) {
	actionPressed = action;
}

void Widget::setActionReleased(AbstractAction * action) {
	actionReleased = action;
}

void Widget::setActionGotFocus(AbstractAction * action) {
	actionGotFocus = action;
}

