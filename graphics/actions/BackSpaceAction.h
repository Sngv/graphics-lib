#ifndef BACKSPACEACTION_H_
#define BACKSPACEACTION_H_

#include "AbstractAction.h"
#include "../widgets/WidgetComponent/TextField.h"

#include <string>
using namespace std;

class BackSpaceAction: public AbstractAction {
private:
	TextField * textField;
public:
	BackSpaceAction(TextField *textField) {
		this->textField = textField;
	}
	void fire() {
		string data = textField->getText();
		if(!data.empty())
			this->textField->setText(data.substr(0, data.size() - 1));
	}
};

#endif /* BACKSPACEACTION_H_ */
