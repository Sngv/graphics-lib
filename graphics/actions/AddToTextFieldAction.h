#ifndef ADDTOTEXTFIELDACTION_H_
#define ADDTOTEXTFIELDACTION_H_

#include "AbstractAction.h"
#include "../widgets/WidgetComponent/TextField.h"

#include <string>


using namespace std;

class AddToTextFieldAction: public AbstractAction {
private:
	TextField *textField;
	string data;
public:
	AddToTextFieldAction(TextField *textField, string data) {
		this->textField = textField;
		this->data = data;
	}

	void fire() {
		this->textField->setText(this->textField->getText() + data);
	}
};

#endif /* ADDTOTEXTFIELDACTION_H_ */
