#ifndef ABSTRACTACTION_H
#define ABSTRACTACTION_H

class AbstractAction {
public:
	virtual void fire()=0;
	virtual ~AbstractAction() {}
};

#endif /* ABSTRACTACTION_H_ */
