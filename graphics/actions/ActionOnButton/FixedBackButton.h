#ifndef FIXEDBACKBUTTON_H_
#define FIXEDBACKBUTTON_H_

#include "../AbstractAction.h"
#include "../../Screen/Screen.h"

class FixedBackButton: public AbstractAction {
public:
	FixedBackButton() {
	}

	void fire() {
		int ID = *Screen::id;
		cout << " :: - > " << ID << endl;
		if (ID != 0 && ID != 1)
			Screen::isActive->at(ID) = 0, Screen::isActive->at(ID - 1) = 1;
	}
};

#endif /* FIXEDBACKBUTTON_H_ */
