#ifndef SWITCHFRAMEACTION_H_
#define SWITCHFRAMEACTION_H_

#include "../actions/AbstractAction.h"
#include "../widgets/WidgetComponent/TextField.h"
#include "../Screen/Screen.h"

#include <string>


using namespace std;

class SwitchFramesAction: public AbstractAction {
	int toFrameID;
public:
	SwitchFramesAction(int toFrameID) {
		this->toFrameID = toFrameID;
	}
	void fire() {
		Screen::isActive->at(*Screen::id) = 0;
		Screen::isActive->at(this->toFrameID) = 1;
	}
};

#endif /* SWITCHFRAMEACTION_H_ */
