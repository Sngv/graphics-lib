#ifndef ALPHABUTTONACTION_H_
#define ALPHABUTTONACTION_H_

#include "AbstractAction.h"
#include "../widgets/WidgetComponent/TextField.h"
#include "../widgets/WidgetComponent/Button.h"

#include <string>


using namespace std;

class AlphaButtonAction: public AbstractAction {
private:
	TextField *textField;
	Button *button;
public:
	AlphaButtonAction(TextField *textField, Button *num) {
		this->textField = textField;
		this->button = num;
	}
	void fire() {
		cout << *Screen::idButtonPress << " // " << *Screen::idButtonprev
				<< endl;
		cout << "charText " << button->charText << endl;
		if (*Screen::idButtonPress == *Screen::idButtonprev) {
			cout << "BUTTONCOUNTER :: " << button->counterPress << endl;
			string tmp = textField->getText();

			if (button->counterPress == 3) {
				button->counterPress = 0;
				tmp[tmp.size() - 1] = button->charText[0] - 1;
			}
			cout << "TMP :: " << tmp << endl;
			if (tmp.size() != 0)
				tmp[tmp.size() - 1]++;
			button->counterPress++;
			textField->setText(tmp);
			*Screen::idButtonprev = *Screen::idButtonPress;

		} else {
			button->counterPress++;
			*Screen::idButtonprev = *Screen::idButtonPress;
			cout << "ACTION HEREEEEEEEEEEEEEEEEEEEEEEee" << endl;
			textField->setText(textField->getText() + button->charText);
		}
	}
};

#endif /* ALPHABUTTONACTION_H_ */
