#ifndef CONSTANTS_H
#define CONSTANTS_H

#include "../driver/graphics.h"
#include "../driver/winbgim.h"

#include <string>
using namespace std;

enum State {
	NORMAL, PRESSED, RELEASED, GOT_FOCUS
};

const int COMMON_HEIGHT = 30;
const int COMMON_MARGIN = 2;


int getNextID();

#endif //CONSTANTS_H
