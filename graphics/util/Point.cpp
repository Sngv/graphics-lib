#include "Point.h" // class's header file
// class constructor
Point::Point(int paramX, int paramY) {
	setX(paramX);
	setY(paramY);
}

// class destructor
Point::~Point() {
	// insert your code here
}

void Point::setX(int paramX) {
	if (paramX >= 0 && paramX <= 800)
		x = paramX;
	else
		x = 0;
}
void Point::setY(int paramY) {
	if (paramY >= 0 && paramY <= 800)
		y = paramY;
	else
		y = 0;
}
int Point::getX() {
	return x;
}
int Point::getY() {
	return y;
}
