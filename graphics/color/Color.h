#ifndef COLOR_H_
#define COLOR_H_

#include "../driver/graphics.h"
#include <string>
using namespace std;

class Color {
public:
	Color();
	virtual ~Color();
	string COLOR_TEXT;
	string OUTER_BORDER;
	string INNER_BORDER;
	string INNER_COLOR;

	// make new instance later
	// just work later
	Color *gotFocus;
	Color *released;
	Color *pressed;

	virtual colors getColor(string colorType);

	colors colorText();
	colors outerBorder();
	colors innerBoard();
	colors innerColor();

};

#endif /* COLOR_H_ */
