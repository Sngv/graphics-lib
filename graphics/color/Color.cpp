// class will configure the color configration with API

#include "Color.h"
#include <iostream>

Color::Color() {

	COLOR_TEXT = "WHITE";
	OUTER_BORDER = "BLACK";
	INNER_BORDER = "WHITE";
	INNER_COLOR = "BLACK";

	gotFocus = this;
	pressed = this;
	released = this;

}

Color::~Color() {
}

colors Color::getColor(string colorType) {

	if (colorType == "BLACK")
		return WHITE;

	if (colorType == "WHITE")
		return BLACK;
}

colors Color::colorText() {
	return getColor(COLOR_TEXT);
}

colors Color::outerBorder() {
	return getColor(OUTER_BORDER);
}

colors Color::innerBoard() {
	return getColor(INNER_BORDER);
}

colors Color::innerColor() {
	return getColor(INNER_COLOR);
}

