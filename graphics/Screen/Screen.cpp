#include "Screen.h"

// static member
Widget * Screen::currentElement = NULL;
Widget *Screen::previousElement = NULL;
Widget *Screen::KeyboardListnerWidget = NULL;
DrawingTools *Screen::drawingTool = new DrawingTools();

// until tommorow find how to make it pointer
//Container<Widget *> *Screen::screenElement = new Container<Widget*>();
vector<Widget *> *Screen::screenElements = new vector<Widget *>;
vector<bool> *Screen::isActive = new vector<bool>;

int *Screen::id = new int(1);
int *Screen::idButtonPress = new int(-1);
int *Screen::idButtonprev = new int(-2);

void Screen::initialize(int width, int height) {
	initwindow(width, height);
	drawingTool->setColorBG(BLUE);
	cleardevice();

	setcolor(WHITE);

	registermousehandler(WM_LBUTTONDOWN, Screen::mousePressed);
	registermousehandler(WM_LBUTTONUP, Screen::mouseReleased);
//	registermousehandler(WM_MOUSEMOVE, Screen::mouseMoved);

	settextstyle(DEFAULT_FONT, HORIZ_DIR, 2);
}

void Screen::ini() {

}

void Screen::destroy() {
	delete currentElement;
	delete previousElement;
	delete KeyboardListnerWidget;
	delete drawingTool;

	for (unsigned int i = 0; i < screenElements->size(); i++)
		delete screenElements->at(i);

	closegraph();
}

void Screen::setSize(int size) {
	drawingTool->setSize(size);
}

void Screen::setFont(font_names font) {
	drawingTool->setFontName(font);
}

void Screen::paint() {
	if (currentElement != NULL)
		currentElement->draw(drawingTool);

	if (previousElement != NULL)
		previousElement->draw(drawingTool);

	previousElement = currentElement;
	for (unsigned int i = 0; i < screenElements->size(); i++) {
		Widget *drawble = screenElements->at(i);
		if (isActive->at(i) != 0 && i != 0)
			*id = i;
		if (isActive->at(i) == 1)
			drawble->draw(drawingTool);
	}
}

void Screen::mouseMoved(int x, int y) {
	if (previousElement != NULL && previousElement->contains(x, y) != NULL
			&& previousElement->contains(x, y)->equals(previousElement)) {
		currentElement = previousElement;
		return;
	}

	currentElement = findPointContainer(x, y);

	if (previousElement != NULL)
		previousElement->setState(NORMAL), previousElement->draw(drawingTool);

	if (currentElement != NULL)
		currentElement->setState(GOT_FOCUS), currentElement->fireAction(
				GOT_FOCUS), currentElement->draw(drawingTool);

	paint();
}

void Screen::mousePressed(int x, int y) {
	cout << "Screen :: mouse Pressed" << endl;
	currentElement = findPointContainer(x, y);

	if (currentElement != NULL)
		currentElement->setState(PRESSED), currentElement->fireAction(PRESSED), currentElement->draw(
				drawingTool);
	paint();
	mouseMoved(x, y);

}

void Screen::mouseReleased(int x, int y) {
	cout << "Screen :: mouse Released" << endl;
	if (currentElement != NULL) {
		currentElement->setState(RELEASED);
		currentElement->fireAction(RELEASED);
		currentElement->draw(drawingTool);
		KeyboardListnerWidget = currentElement;
	}
	paint();
	mouseMoved(x, y);

}

DrawingTools *Screen::getDrawingTool() {
	return drawingTool;
}

Widget * Screen::findPointContainer(int x, int y) {
	for (unsigned int i = 0; i < screenElements->size(); i++) {
		Widget *drawble = screenElements->at(i);
		Widget *temp = drawble->contains(x, y);
		cout << Screen::isActive->at(i) << endl;
		if (temp != NULL && Screen::isActive->at(i) == 1) {
			return temp;
		}
	}
	return NULL;
}

void Screen::add(Widget *drawble) {
	screenElements->push_back(drawble);
	isActive->push_back(0);
}

void Screen::show() {
	char c;
	isActive->at(0) = 1;
	isActive->at(1) = 1;
	Screen::paint();
	while (!kbhit()) {
		c = (char) getch();
		cout << c;
//		if (KeyboardListnerWidget != NULL) {
//			KeyboardListnerWidget->handleKeyboardInput(c);
//			KeyboardListnerWidget->draw(drawingTool);
//		}
	}
}
