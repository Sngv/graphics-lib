#ifndef SCREEN_H_
#define SCREEN_H_

#include <vector>

#include "../driver/winbgim.h"  // Provides the BGI graphics functions for Windows
#include "../driver/graphics.h"

#include "../DrawingTools/DrawingTools.h"
#include <iostream>
#include "../util/Point.h"
#include "../widgets/Widget.h"

class Screen {
private:
	static Point lastPoint;
	static Point firstPoint;

	static Widget *currentElement;
	static Widget *previousElement;
	static Widget *KeyboardListnerWidget;

	static DrawingTools *drawingTool;

public:

	// implementing vector with your hand waiting abdelgawad

	static vector<Widget *> * screenElements;
	static vector<bool> *isActive;

	static int *id;
	static int *idPrev;
	static int *idButtonPress;
	static int *idButtonprev;

	static void initialize(int width, int height);
	static void destroy();
	static void ini();
	static void setSize(int size);
	static void setFont(font_names font);

	static void paint();
	static void mouseMoved(int x, int y);
	static void mousePressed(int x, int y);
	static void mouseReleased(int x, int y);

	static DrawingTools *getDrawingTool();

	static Widget * findPointContainer(int x, int y);
	static void add(Widget * drawble);
	static void show();
};

#endif /* SCREEN_H_ */
