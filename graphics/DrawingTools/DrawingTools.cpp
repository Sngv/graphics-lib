#include "DrawingTools.h"

DrawingTools::DrawingTools() {

}

DrawingTools::~DrawingTools() {

}

void DrawingTools::setSize(int size) {
	this->size = size;
	settextstyle(DEFAULT_FONT, HORIZ_DIR, size);
}

void DrawingTools::setFontName(font_names font) {
	this->font = font;
	settextstyle(font, HORIZ_DIR, size);
}

void DrawingTools::drawText(char *msg, int x, int y) {
	outtextxy(x, y, msg);
}

void DrawingTools::drawLine(int x1, int y1, int x2, int y2) {
	line(x1, y1, x2, y2);
}

void DrawingTools::drawRectangle(int x1, int y1, int x2, int y2) {
	bar(x1, y1, x2, y2), rectangle(x1, y1, x2, y2);
}

void DrawingTools::drawCircle(int x, int y, int radious) {
	circle(x, y, radious);
}

void DrawingTools::clearDevice() {
	clearDevice();
}

void DrawingTools::clearViewport() {
	clearViewport();
}

void DrawingTools::setColorFG(colors color) {
	setcolor(color);
}

void DrawingTools::setColorBG(colors color) {
	setfillstyle(SOLID_FILL, color), setbkcolor(color);
}
