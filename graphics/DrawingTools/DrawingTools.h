#ifndef DRAWINGTOOLS_H_
#define DRAWINGTOOLS_H_
#include "../driver/winbgim.h"  // Provides the BGI graphics functions for Windows
#include "../driver/graphics.h"

#include "string"
using namespace std;
class DrawingTools {
protected:

public:
	int size;
	font_names font;
	// drawing tools
	DrawingTools();
	~DrawingTools();
	void drawText(char *msg, int x, int y);
	void drawLine(int x1, int y1, int x2, int y2);
	void drawRectangle(int x1, int y1, int x2, int y2);
	void drawCircle(int x, int y, int radius);

	void clearDevice();
	void clearViewport();

	// customization
	void setSize(int size);
	void setFontName(font_names font);
	void setTextStyle(int font, int charSize);
	void setColorFG(colors color);
	void setColorBG(colors color);

};

#endif /* DRAWINGTOOLS_H_ */
