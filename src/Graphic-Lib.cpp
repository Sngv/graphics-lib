#include "../graphics/Screen/Screen.h"
#include "../graphics/DrawingTools/DrawingTools.h"
#include "../graphics/widgets/Widget.h"
#include "../graphics/widgets/WidgetComponent/Frame.h"
#include "../graphics/util/Point.h"
#include "../graphics/widgets/WidgetComponent/Button.h"
#include "../graphics/widgets/WidgetComponent/TextField.h"
#include "../graphics/actions/AddToTextFieldAction.h"
#include "../graphics/actions/BackSpaceAction.h"
#include "../graphics/UI/Menu/HomeMenu.h"
#include "../graphics/UI/Menu/FixedMenu.h"
#include "../graphics/UI/KeyPad/NumericKeypad.h"
#include "../graphics/UI/Calling.h"
#include "../graphics/UI/KeyPad/AlphaKeypad.h"
#include "../graphics/UI/SendSMS.h"

using namespace std;

int main() {

	Screen::initialize(240, 320);

	FixedMenu *menu = new FixedMenu();

	HomeMenu * home = new HomeMenu();
	home->setUpperLeft(0, 0);
	home->setWidth(240);
	home->setHeight(280);

	NumericKeypad *numericKeyPad = new NumericKeypad();
	numericKeyPad->setUpperLeft(0, 0);
	numericKeyPad->setWidth(240);
	numericKeyPad->setHeight(280);

	AlphaKeypad *alphaKey = new AlphaKeypad();
	alphaKey->setUpperLeft(0, 0);
	alphaKey->setWidth(240);
	alphaKey->setHeight(280);

	Calling *call = new Calling();

	SendSMS *sms = new SendSMS();

	Screen::add(menu);
	Screen::add(home);
	Screen::add(call);
	Screen::add(numericKeyPad);
	Screen::add(alphaKey);
	Screen::add(sms);

	home->AddActionToButton();
	menu->AddActionToButton();
	numericKeyPad->AddActionToButton();
	alphaKey->AddActionToButton();

	cout << Screen::isActive->size() << endl;

	Screen::show();
	return 0;
}
